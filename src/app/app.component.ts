import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
// this is the new page
import { Chatpg } from '../pages/chatpg/chatpg';
import {OneSignal} from "@ionic-native/onesignal";



@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  //rootPage: any = HomePage;

  pages: Array<{title: string, component: any}>;
  hideSplash: boolean = false;
  // hideLoader: boolean = true;

  // private oneSignal: OneSignal, - make you add this to the constructor

  constructor(public platform: Platform, public statusBar: StatusBar, private oneSignal: OneSignal,
    public splashScreen: SplashScreen) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Notifications', component: HomePage },
      { title: 'Contacts', component: ListPage },
      { title: 'Chat', component: Chatpg }
    ];
    setTimeout(()=>{
      this.hideSplash = true;
      // this.hideLoader = false;
      this.nav.setRoot(HomePage);
    }, 5000);
    
    // setTimeout(()=>{
    //   this.hideLoader = false;
    //   this.nav.setRoot(HomePage);
    // }, 6000);

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();


    // OneSignal Code starts here:
    // Enable to debug issues:
    // window["plugins"].OneSignal.setLogLevel({logLevel: 4, visualLevel: 4});
    //Don't for get to run: npm install @ionic-native/onesignal

    this.oneSignal.startInit('9589c6fe-4df9-4fbd-9701-d85bc78ea105', '553832677044');

    this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);

    this.oneSignal.handleNotificationReceived().subscribe(() => {
        console.log('notification received');
        
        // do something when notification is received
    });

    this.oneSignal.handleNotificationOpened().subscribe(() => {
        console.log('notification opened');
        // do something when a notification is opened
    });

    this.oneSignal.endInit();
    // // OneSignal Code ends here: 


    });

      

    

    };


  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
