import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Chatpg } from '../chatpg/chatpg';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  // showPanel is how to declare variable in ionic
  //the things that look really really difficult are really really simple. 
  
  showPanel: boolean = false;
  @ViewChild('articleText') aText: ElementRef
  constructor(public navCtrl: NavController) {
    this.showPanel = true;
    //var rr = this.aText.nativeElement.outerHeight();
    console.log(this.aText);
  }

  togglePanel(){
    this.showPanel = !this.showPanel;
  }

  movetochat(){
    this.navCtrl.push(Chatpg);

  }
  // untogglePanel(){
  //   this.showPanel = this.showPanel;
  // }

}
