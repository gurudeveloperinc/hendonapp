import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Chatpg } from './chatpg';

@NgModule({
  declarations: [
    Chatpg,
  ],
  imports: [
    IonicPageModule.forChild(Chatpg),
  ],
})
export class ChatpgModule {}
